function Node (id, data) {
    this.id = id;
    this.vectors = [];
    this.data = data;
}

function Vector (id, target, data) {
    this.id = id;
    this.target = target;
    this.data = data;
}

function Edge (id, lv, rv, data) {
    this.id = id;
    this.lv = lv;
    this.rv = rv;
    this.data = data;
}

function Graph () {
    this._nodes = {};
    this._vectors = {};
    this._edges = {};

    this._nodesCount = 0;
    this._vectorsCount = 0;
    this._edgesCount = 0;
}

Graph.prototype.addNode = function (id, data) {
    var node = this._nodes[id];

    if (typeof node === 'undefined') {
        node = new Node(id, data);
        this._nodes[id] = node;
        this._nodesCount++;
    }

    return node;
}

Graph.prototype.removeNode = function (id) {
    if (!this.hasNode(id)) return;
    delete this._nodes[id];
    this._nodesCount--;
}

Graph.prototype.getNode = function (id) {
    return this._nodes[id];
}

Graph.prototype.hasNode = function (id) {
    return typeof this._nodes[id] === 'object';
}

Graph.prototype.getAllNodes = function () {
    return Object.keys(this._nodes).map((key) => {
        return this._nodes[key];
    })
}

Graph.prototype.getNodesCount = function () {
    return this._nodesCount;
}

Graph.prototype.addVector = function (sourceId, targetId, data) {
    var vectorId = this._concatId(sourceId, targetId);
    var vector = this._vectors[vectorId];

    if (typeof vector === 'undefined') {
        var source = this.getNode(sourceId);
        var target = this.getNode(targetId);

        if (!source || !target) {
            return undefined;
        }

        vector = new Vector(vectorId, target, data);
        source.vectors.push(vector);

        this._vectors[vectorId] = vector;
        this._vectorsCount++;
    }

    return vector;
}

Graph.prototype.removeVector = function (sourceId, targetId) {
    var vectorId = this._concatId(sourceId, targetId);
    var vector = this._vectors[vectorId];

    if (typeof vector === 'object') {
        var source = this.getNode(sourceId);

        delete this._vectors[vectorId];
        this._vectorsCount--;

        if (!source || !this.hasNode(targetId)) {
            return undefined;
        }

        for (var i = 0, ii = source.vectors.length; i < ii; i++) {
            if (source.vectors[i].target.id == targetId) {
                source.vectors.splice(i, 1);
            }
        }
    }
}

Graph.prototype.getVector = function (sourceId, targetId) {
    var id = this._concatId(sourceId, targetId);
    return this._vectors[id];
}

Graph.prototype.hasVector = function (sourceId, targetId) {
    var id = this._concatId(sourceId, targetId);
    return typeof this._vectors[id] === 'object';
}

Graph.prototype.getAllVectors = function () {
    return Object.keys(this._vectors).map((key) => {
        return this._vectors[key];
    })
}

Graph.prototype.getVectorsCount = function () {
    return this._vectorsCount;
}

Graph.prototype.addEdge = function (leftNodeId, rightNodeId, data) {
    var edgeId = this._compoundId(leftNodeId, rightNodeId);
    var edge = this._edges[edgeId];

    if (typeof edge === 'undefined') {
        var leftNode = this.getNode(leftNodeId);
        var rightNode = this.getNode(rightNodeId);

        if (!leftNode || !rightNode) {
            return undefined;
        }

        var lv = this.addVector(leftNodeId, rightNodeId);
        var rv = this.addVector(rightNodeId, leftNodeId);
        edge = new Edge(edgeId, lv, rv, data);

        this._edges[edgeId] = edge;
        this._edgesCount++;
    }

    return edge;
}

Graph.prototype.removeEdge = function (leftNodeId, rightNodeId) {
    var edgeId = this._compoundId(leftNodeId, rightNodeId);
    var edge = this._edges[edgeId];

    if (typeof edge === 'object') {
        var leftNode = this.getNode(leftNodeId);

        this.removeVector(leftNodeId, rightNodeId);
        this.removeVector(rightNodeId, leftNodeId);

        delete this._edges[edgeId];
        this._edgesCount--;
    }
}

Graph.prototype.getEdge = function (leftNodeId, rightNodeId) {
    var edgeId = this._compoundId(leftNodeId, rightNodeId);
    return this._edges[edgeId];
}

Graph.prototype.hasEdge = function (leftNodeId, rightNodeId) {
    var edgeId = this._compoundId(leftNodeId, rightNodeId);
    return typeof this._edges[edgeId] === 'object';
}

Graph.prototype.getAllEdges = function () {
    return Object.keys(this._edges).map((key) => {
        return this._edges[key];
    })
}

Graph.prototype.getEdgesCount = function () {
    return this._edgesCount;
}


Graph.prototype.getAdjacentVectors = function (id) {
    var n = this.getNode(id);

    if (!n || !n.vectors) {
        return undefined;
    }

    return n.vectors;
}

Graph.prototype.getAdjacentEdges = function (id) {
    var adjVectors = this.getAdjacentVectors(id);
    return adjVectors.map((v) => {
        return this.getEdge(id, v.target.id);
    })
}

Graph.prototype.getAdjacentNodes = function (id) {
    var adjVectors = this.getAdjacentVectors(id);
    return adjVectors.map((v) => {
        return v.target
    })
}

Graph.prototype._concatId = function (s1, s2) {
    return this._hashCode('' + s1 + s2);
}

Graph.prototype._compoundId = function (s1, s2) {
    if (s1 > s2) {
        var tmp = s1;
        s1 = s2;
        s2 = tmp;
    }

    return this._hashCode('' + s1 + s2);
}

Graph.prototype._hashCode = function (s) {
    return s.split('').reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0);
}

module.exports = Graph
