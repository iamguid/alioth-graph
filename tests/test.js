var expect = require('chai').expect
var Graph = require('../lib/Graph.js')

describe('Alioth graph test', function () {
    describe('Nodes manipulation', function () {
        var g = new Graph();

        it('#addNode()', function () {
            g.addNode('n1', 3);
            g.addNode('n2', 2);
            var n1 = g.getNode('n1');
            var n2 = g.getNode('n2');
            expect(n1.data).to.equal(3);
            expect(n2.data).to.equal(2);
        });

        it('#getNode()', function () {
            var existNode = g.getNode('n2');
            var notExistNode = g.getNode('notExistNode');
            expect(existNode).to.not.be.undefined;
            expect(notExistNode).to.be.undefined;
        });

        it('#hasNode()', function () {
            expect(g.hasNode('n1')).to.be.true;
            expect(g.hasNode('n3')).to.be.false;
        });

        it('#getAllNodes()', function () {
            var nodes = g.getAllNodes();
            var ids = nodes.map(function (n) { return n.id });
            expect(ids).to.have.members(['n1', 'n2']);
        });

        it('#removeNode()', function () {
            g.removeNode('n2');
            expect(g.getNode('n2')).to.be.undefined;
        });

        it('#getNodesCount()', function () {
            var cnt = g.getNodesCount();
            expect(cnt).to.equal(1);
        });
    });

    describe('Vectors manipulation', function () {
        var g = new Graph();

        it('#addVector()', function () {
            var n1 = g.addNode('n1');
            var n2 = g.addNode('n2');
            var v1 = g.addVector('n1', 'n2');
            expect(v1.target.id).to.equal('n2');
        });

        it('#hasVector()', function () {
            expect(g.hasVector('n1', 'n2')).to.be.true;
            expect(g.hasVector('n2', 'n3')).to.be.false;
        });

        it('#getAllVectors()', function () {
            var vectors = g.getAllVectors();
            expect(vectors).to.have.lengthOf(1);
        });

        it('#getVectorsCount()', function () {
            var cnt = g.getVectorsCount();
            expect(cnt).to.equal(1);
        });

        it('#removeVector()', function () {
            g.removeVector('n1', 'n2');
            expect(g.hasVector('n1', 'n2')).to.be.false;
            expect(g.getVectorsCount()).to.equal(0);
        });
    });

    describe('Edges manipulation', function () {
        var g = new Graph();

        it('#addEdge()', function () {
            var n1 = g.addNode('n1');
            var n2 = g.addNode('n2');
            var e1 = g.addEdge('n1', 'n2');
            expect(g.hasVector('n1', 'n2')).to.be.true;
            expect(g.hasVector('n2', 'n1')).to.be.true;
            expect(g.hasEdge('n2', 'n1')).to.be.true;
        });

        it('#hasEdge()', function () {
            expect(g.hasEdge('n2', 'n1')).to.be.true;
            expect(g.hasEdge('n1', 'n2')).to.be.true;
            expect(g.hasEdge('n1', 'n3')).to.be.false;
        });

        it('#getAllEdges()', function () {
            var edges = g.getAllEdges();
            expect(edges).to.have.lengthOf(1);
        });

        it('#getEdgesCount()', function () {
            var cnt = g.getEdgesCount();
            expect(cnt).to.equal(1);
        });

        it('#removeEdge()', function () {
            g.removeEdge('n1', 'n2');
            expect(g.hasVector('n1', 'n2')).to.be.false;
            expect(g.hasVector('n2', 'n1')).to.be.false;
            expect(g.hasEdge('n2', 'n1')).to.be.false;
            expect(g.getEdgesCount()).to.equal(0);
            expect(g.getVectorsCount()).to.equal(0);
        });
    });

    describe('Adjacent', function () {
        var g = new Graph();

        g.addNode('n1');
        g.addNode('n2');
        g.addNode('n3');
        g.addNode('n4');

        g.addEdge('n1', 'n2')
        g.addEdge('n1', 'n3')
        g.addVector('n3', 'n4')

        it('#getAdjacentVectors()', function () {
            var adj1 = g.getAdjacentVectors('n1');
            var ids1 = adj1.map(function (v) { return v.target.id });
            var adj2 = g.getAdjacentVectors('n3');
            var ids2 = adj2.map(function (v) { return v.target.id });
            expect(ids1).to.have.members(['n2', 'n3']);
            expect(ids2).to.have.members(['n1', 'n4']);
        });

        it('#getAdjacentNodes()', function () {
            var adj1 = g.getAdjacentNodes('n1');
            var ids1 = adj1.map(function (n) { return n.id });
            var adj2 = g.getAdjacentNodes('n2');
            var ids2 = adj2.map(function (n) { return n.id });
            expect(ids1).to.have.members(['n2', 'n3']);
            expect(ids2).to.have.members(['n1']);
        });

        it('#getAdjacentEdges()', function () {
            var adj = g.getAdjacentEdges('n1');
            expect(adj).to.have.lengthOf(2);
        });
    });
});
